export const _isEmpty = (arr) => arr.length < 1;

export const _isEqualLength = (a, b) => a.length === b.length;

export const _showOnlyCorrect = ({ isCorrect }) => isCorrect == true;

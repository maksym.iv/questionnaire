import { useState, useContext } from 'react';
import { Questions, Modal, Results, Starter } from './components';
import { ThemeContext } from "./contexts/theme";
import './index.scss'

function App() {
  const [isDark] = useContext(ThemeContext);
  const [results, setResults] = useState([]);
  const [step, setStep] = useState(0);

  const theme = isDark ? 'dark' : 'light';

  const finishHandler = () => setStep(2);

  const startAgain = () => {
    setResults([]);
    setStep(1);
  };
  const toMainPage = () => {
    setResults([]);
    setStep(0);
  };
  const startHandler = () => setStep(1);

  const renderStep = () => {
    switch (step) {
      case 0:
        return <Starter startHandler={startHandler} />
      case 1:
        return <Questions setResult={setResults} finishHandler={finishHandler} />
      case 2:
        return <Results results={results} startAgain={startAgain} toMainPage={toMainPage} />
    };
  };
  
  return (
    <div className="app" data-theme={theme}>
      <Modal>
        {renderStep()}
      </Modal>
    </div>
  );
}

export default App;

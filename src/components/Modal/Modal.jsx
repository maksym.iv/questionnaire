import { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from "../../contexts/theme";
import flag from '../../assets/images/flag.jpeg';

function Modal({ children }) {
    const [isDark, toggleTheme] = useContext(ThemeContext);
    const theme = isDark ? 'dark' : 'light';
    
    return (
        <div className="Modal">
            <img src={flag} alt='flag' />
            <div className='titleRow'>
                <p>US Quiz</p>
                <label htmlFor="toggler">
                    <p>{`${theme} theme`}</p>
                    <input type="checkbox" id="toggler" checked={isDark} className='toggler' onChange={toggleTheme} />
                </label>
            </div>
            {children}
        </div>
    );
}

Modal.propTypes = {
    children: PropTypes.node
};

export default Modal;
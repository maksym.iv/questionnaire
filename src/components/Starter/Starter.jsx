import PropTypes from 'prop-types';

const Starter = ({ startHandler }) => (
    <div className='Starter'>
        <h3>
            Welcome to the US QUIZ
        </h3>
        <p>
            Take a quiz for all ages. Test your knowledge!
        </p>

        <div className='footer'>
            <button onClick={startHandler}>NEXT</button>
        </div>
    </div>
);

Starter.propTypes = {
    startHandler: PropTypes.func,
};

export default Starter;
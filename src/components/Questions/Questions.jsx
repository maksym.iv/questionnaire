import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import data from '../../mocks/data';
import { _isEmpty, _isEqualLength, _showOnlyCorrect } from '../../utils';

function Questions({  setResult, finishHandler }) {
    const [step, setStep] = useState(0);
    const [selectedAnswer, setSelectedAnswer] = useState([]);
    const [error, setError] = useState('');
    const formWrapper = useRef();
    const answers = data[step].answers;

    useEffect(() => {
        formWrapper.current.querySelectorAll('input').forEach(el => el.checked = false);
    }, [answers]);
    
    const isMultiply = answers.filter(_showOnlyCorrect).length > 1;
    const formType = isMultiply ? "checkbox" : "radio";
    const correctAnswers = answers.filter(_showOnlyCorrect);
    const userAnswers = answers.filter(({ text }) => {
        if (isMultiply) return selectedAnswer.includes(text);
        return text === selectedAnswer;
    });
    const isCheckedAll = _isEqualLength(correctAnswers, userAnswers);
    const isCorrect = isCheckedAll && userAnswers.every(_showOnlyCorrect);

    const nextStep = () => {
        if (_isEmpty(selectedAnswer)) return setError('Please select one option!');
        setSelectedAnswer([]);

        setResult(prev => [...prev, { step, isCorrect }]);
        if (step === data.length - 1) {
            finishHandler(true);
        } else {
            setStep(prev => prev + 1);
        }
    };

    const changeHandler = ({ target: { value } }) => {
        setSelectedAnswer(prev => isMultiply ? [...prev, value] : value);
        if (error) {
            setError('');
        }
    };

    return (
        <div className='Questions'>
            <h3>
                Question №{step + 1}
                <span>
                    {isMultiply ? ' (You can choose multiple options)' : ''}
                </span>
            </h3>
            <h4>
                {data[step].question}
            </h4>
            <div className="container" ref={formWrapper}>
                {answers.map(({ text }, i) => (
                    <label key={`answer-${i}`}>
                        <input type={formType} name="answer" value={text} onChange={changeHandler} />
                        {text}
                    </label>

                ))}
            </div>
            <div className='footer'>
                {error && <p className="error">{error}</p>}
                <button onClick={nextStep}>NEXT</button>
            </div>
        </div>
    );
}

Questions.propTypes = {
    setResult: PropTypes.func,
    finishHandler: PropTypes.func
};

export default Questions;

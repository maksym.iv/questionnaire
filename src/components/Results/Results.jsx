import PropTypes from 'prop-types';
import { _showOnlyCorrect } from '../../utils';

function Results({ results, toMainPage, startAgain }) {
    const getCorrect = (isCorrect) => isCorrect ? 'correct' : 'wrong';
    const mark = results.filter(_showOnlyCorrect).length
    return (
        <div className='Results'>
            <h3>Correct answers {mark} in {results.length} questions</h3>
            <ul>
                {results.map(({ step, isCorrect }) => (
                    <li key={`res-${step}`}>
                        <p>Question {step + 1}:</p>
                        <p className={getCorrect(isCorrect)}>{getCorrect(isCorrect)}</p>
                    </li>
                ))}
            </ul>

            <div className='footer'>
                <button onClick={toMainPage}>to main page</button>
                <button onClick={startAgain}>start again</button>
            </div>
        </div>
    );
};

Results.propTypes = {
    results: PropTypes.array,
    toMainPage: PropTypes.func,
    startAgain: PropTypes.func
};

export default Results;
